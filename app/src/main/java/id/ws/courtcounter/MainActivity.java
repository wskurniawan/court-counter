package id.ws.courtcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView txtSkorA, txtSkorB;
    Button btnAdd3A, btnAdd2A, btnAdd1A, btnAdd3B, btnAdd2B, btnAdd1B;

    private int skorA = 0;
    private int skorB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtSkorA = findViewById(R.id.txt_skor_a);
        txtSkorB = findViewById(R.id.txt_skor_b);
        btnAdd1A = findViewById(R.id.btn_add_1_a);
        btnAdd1B =findViewById(R.id.btn_add_1_b);
        btnAdd2A = findViewById(R.id.btn_add_2_a);
        btnAdd2B = findViewById(R.id.btn_add_2_b);
        btnAdd3A = findViewById(R.id.btn_add_3_a);
        btnAdd3B = findViewById(R.id.btn_add_3_b);
    }

    public void add1A(View view){
        skorA++;
        updateDisplay();
    }

    public void add2A(View v){
        skorA = skorA + 2;
        updateDisplay();
    }

    public void add3A(View v){
        skorA = skorA + 3;
        updateDisplay();
    }

    public void add1B(View view){
        skorB++;
        updateDisplay();
    }

    public void add2B(View v){
        skorB = skorB + 2;
        updateDisplay();
    }

    public void add3B(View v){
        skorB = skorB + 3;
        updateDisplay();
    }

    public void btnReset(View v){
        skorA = 0;
        skorB = 0;
        updateDisplay();
    }

    private void updateDisplay(){
        txtSkorA.setText(String.valueOf(skorA));
        txtSkorB.setText(String.valueOf(skorB));
    }
}
